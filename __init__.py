# -*- coding: utf-8 -*-
#import sys  # for devel only
#
#import numpy
#import json
#
from aiida.parsers.parser import Parser
from aiida.parsers.exceptions import OutputParsingError
from aiida.common.datastructures import calc_states
#
from aiida.orm.calculation.job.vasp.vasp import VaspCalculation
from aiida.orm.calculation.job.vasp.vasp import ParserInstructionFactory
from aiida.orm.data.parameter import ParameterData

__copyright__ = u'Copyright © 2016, Mario Zic, Trinity College Dublin. All Rights Reserved.'
__license__ = "Apache, Version 2.0, see LICENSE.txt file"
__version__ = "0.0.1"
__contributors__ = "Mario Zic"
__contact__ = u'zicm_at_tcd.ie'


class VaspParser(Parser):
    """
    This class is the implementation of the Parser class
    for the VASP calculator.
    """
    _outstruct_name = 'output_structure'

    def __init__(self, calculation):
        """
        Initialize the instance of VaspParser
        """
        # check for valid input
        if not isinstance(calculation, VaspCalculation):
            raise OutputParsingError(
                "Input calculation must be a VaspCalculation"
            )
        self._calc = calculation

    def parse_from_calc(self, manual=False):
        """
        Parses the datafolder, stores results.
        """
        from aiida.common.exceptions import InvalidOperation
        from aiida.common import aiidalogger
        from aiida.djsite.utils import get_dblogger_extra
        parserlogger = aiidalogger.getChild('vaspparser')
        logger_extra = get_dblogger_extra(self._calc)

        # suppose at the start that the job is successful
        successful = True
        parser_warnings = {}  # for logging non-critical events

        # check that calculation is in the right state
        if not manual:
            state = self._calc.get_state()
            if state != calc_states.PARSING:
                raise InvalidOperation(
                    "Calculation not in {} state".format(calc_states.PARSING)
                )

        # get parser instructions
        # TODO: output parser should NOT interpret the input !!!
        try:
            instruct = self._calc.get_inputs_dict().pop(
                self._calc.get_linkname('settings'))
            instruct = instruct.get_dict()
            instruct = instruct[u'PARSER_INSTRUCTIONS']

            # check if structure, data, and error parsers are specified
            # if not append defaults
            itypes = [i['type'] for i in instruct]
            # structure
            if not 'structure' in itypes:
                instruct.append({
                    'instr': 'default_structure_parser',
                    'type': 'structure',
                    'params': {}}
                )
                parser_warnings.setdefault(
                    'Structure parser instruction not found!',
                    'default_structure_parser loaded.'
                )
            # error
            if not 'error' in itypes:
                instruct.append({
                    'instr': 'default_error_parser',
                    'type': 'error',
                    'params': {}}
                )
                parser_warnings.setdefault(
                    'Error parser instruction not found!',
                    'default_error_parser loaded.'
                )
            # output
            if not 'data' in itypes:
                instruct.append({
                    'instr': 'default_vasprun_parser',
                    'type': 'data',
                    'params': {}}
                )
                parser_warnings.setdefault(
                    'Data parser instruction not found!',
                    'default_data_parser_parser loaded.'
                )
        except:
            parser_warnings.setdefault(
                'Parser instructions not found',
                'Default instructions were loaded.'
            )
            # don't crash, load default instructions instead
            instruct = [
                # output
                {
                    'instr': 'default_vasprun_parser',
                    'type': 'data',
                    'params': {}
                },
                # error
                {
                    'instr': 'default_error_parser',
                    'type': 'error',
                    'params': {}
                },
                # structure
                {
                    'instr': 'default_structure_parser',
                    'type': 'structure',
                    'params': {}
                }
            ]

        # select the folder object
        out_folder = self._calc.get_retrieved_node()

        # check what is inside the folder
        list_of_files = out_folder.get_folder_list()

        # === check if mandatory files exist ===
        # default output file should exist
        if not self._calc._default_output in list_of_files:
            successful = False
            parserlogger.error(
                "Standard output file ({}) not found".format(
                    self._calc._default_output
                ),
                extra=logger_extra
            )
            return successful, ()
        # output structure file should exist
        if not self._calc._output_structure in list_of_files:
            successful = False
            parserlogger.error(
                "Output structure file ({}) not found".format(
                    self._calc._output_structure
                ),
                extra=logger_extra
            )
            return successful, ()
        # stderr file should exist
        if not self._calc._SCHED_ERROR_FILE in list_of_files:
            successful = False
            parserlogger.error(
                "STDERR file ({}) not found".format(
                    self._calc._SCHED_ERROR_FILE
                ),
                extra=logger_extra
            )
            return successful, ()

        instr_node_list = []
        errors_node_list = []

        # === execute instructions ===
        for instr in instruct:
            # create an executable instruction
            try:
                # load instruction
                itype = instr['type'].lower()
                iname = instr['instr']
                iparams = instr['params']
                ifull_name = "{}.{}".format(itype, iname)

                debug_step = 0
                # append parameters
                if itype == 'error':
                    iparams.setdefault(
                        'SCHED_ERROR_FILE', self._calc._SCHED_ERROR_FILE)
                elif itype == 'structure':
                    iparams.setdefault(
                        'OUTPUT_STRUCTURE', self._calc._output_structure)
                debug_step = 1
                # instantiate
                instr = ParserInstructionFactory(ifull_name)
                debug_step = 2
                # raise ValueError('test')
                instr_exe = instr(
                    out_folder,
                    params=iparams if iparams else None
                )
                debug_step = 3
            except ValueError as e:
                # use "full" instruction name
                # to avoid potential name conflicts
                tmp = ifull_name.replace('.', '/')  # avoid "." in the key
                parser_warnings.setdefault(
                    '{}_instruction'.format(tmp),
                    ('Invalid parser instruction - could not be instantiated! '
                        'Error: {}, '
                        'DEBUG STEP: {}'.format(e, debug_step))
                )
                instr_exe = None

            # execute
            if instr_exe:
                try:
                    item_list = instr_exe.execute()
                    for item in item_list:  # store the results
                        if '.' in item[0]:  # check key value
                            raise ValueError(item)

                        instr_node_list.append(item)

                except ValueError as e:
                    tmp = ifull_name.replace('.', '/')  # avoid "." in the key
                    parser_warnings.setdefault('output', {})  # in case it doesn't exist
                    parser_warnings['output'].setdefault(
                        '{}_instruction'.format(tmp),
                        'Found bad key name - skipping! Node: {}'.format(e)
                    )
                except Exception as e:
                    tmp = ifull_name.replace('.', '/')  # avoid "." in the key
                    parser_warnings.setdefault('output', {})  # in case it doesn't exist
                    parser_warnings['output'].setdefault(
                        '{}_instruction'.format(tmp),
                        'Failed to execute. Errors: {}'.format(e)
                    )

        # add all parser warnings to the error list
        try:
            tmp = ParameterData(dict=parser_warnings)
            # raise OutputParsingError('test')  # simulate crash
            parser_warnings = tmp
        except Exception as e:
            parser_warnings = {"create_parser_warnings_failed": "{}".format(e),
                               "parser_warnings_content": parser_warnings}
            parser_warnings = ParameterData(dict=parser_warnings)

        errors_node_list.append((
            'parser_warnings', parser_warnings
        ))

        # === parse the output parameters ===
        # TODO: see what needs to be inside the 'output_params'
        # if the default parser was executed I should reuse the data
        # make this also an instruction so we can modify it later easily
        output_params = {'hello': 'hello from output_params'}

        # === save the outputs ===
        new_nodes_list = []

        # save the errors/warrnings
        for item in errors_node_list:
            new_nodes_list.append(item)

        # save vasp data
        if instr_node_list:
            for item in instr_node_list:
                new_nodes_list.append(item)

        # save output parameters for the ResultManager
        output_params = ParameterData(dict=output_params)
        new_nodes_list.append(
            (self.get_linkname_outparams(), output_params))

        return successful, new_nodes_list
